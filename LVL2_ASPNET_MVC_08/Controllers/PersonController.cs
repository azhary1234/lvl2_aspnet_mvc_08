﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace LVL2_ASPNET_MVC_08.Controllers
{
    public class PersonController : Controller
    {
        // GET: Person
        public ActionResult Index()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            ReportParameter[] reportParameters = new ReportParameter[1];
            reportParameters[0] = new ReportParameter("Firstname", "Ken");
            report.ServerReport.ReportPath = "/PersonReport";
            report.ServerReport.SetParameters(reportParameters);
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;

            return View();
        }
    }
}