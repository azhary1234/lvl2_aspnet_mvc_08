﻿using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNET_MVC_08
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
